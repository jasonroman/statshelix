<?php

namespace AppBundle\Service;

/**
 * Class for comparing game events
 */
class GameEventComparison
{
    /**
     * Compare a list of events against an X/Y coordinate and return a sorted list of most similar events
     *
     * @param array $events
     * @param float $x
     * @param float $y
     * @param int|null $numToReturn number of events to return - leave null to return all events
     * @return array
     */
    public function compareToPosition(array $events, $x, $y, $numToReturn = null)
    {
        // get the count of the number of events and how many events to return
        $numEvents      = count($events);
        $numToReturn    = ($numToReturn) ?: $numEvents;

        // retrieve the distance of every event
        for ($i = 0; $i < $numEvents; $i++) {
            $events[$i]['distance'] = $this->getDistance($events[$i]['location_x'], $x, $events[$i]['location_y'], $y);
        }

        // sort the events based on the distance key
        usort($events, array($this, 'compareEventsByDistance'));

        return array_slice($events, 0, $numToReturn);
    }

    /**
     * Function used to compare 2 events by distance, used for sorting
     *
     * @param array $event1
     * @param array $event2
     * @return int
     */
    public function compareEventsByDistance($event1, $event2)
    {
        // could use <=> if this were PHP 7
        if ($event1['distance'] == $event2['distance']) {
            return 0;
        }

        return $event1['distance'] < $event2['distance'] ? -1 : 1;
    }

    /**
     * Find the Euclidean distance between two points
     *
     * Might be better to do $x1, $y1, $x2, $y2, or maybe create a Point object and pass it two points
     * 
     * @param float $x1
     * @param float $x2
     * @param float $y1
     * @param float $y2
     */
    public function getDistance($x1, $x2, $y1, $y2)
    {
        return sqrt(pow(($x2 - $x1), 2) + pow(($y2 - $y1), 2));
    }
}