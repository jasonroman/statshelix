<?php

namespace AppBundle\Service;

/**
 * Class for reading game events
 */
class GameEventReader
{
    /**
     * Read a CSV of game events
     *
     * @param string $filename
     * @param bool $ignoreHeaderRow
     * @return array
     */
    public function readCsv($filename, $ignoreHeaderRow = true)
    {
        $i          = 0;
        $events     = [];
        $filePath   = __DIR__.'/data/'.$filename;

        // could be dangerous just allowing any filename to be read
        if (($handle = fopen($filePath, 'r')) === FALSE) {
            throw new \InvalidArgumentException('Could not read CSV file');
        }

        while (($data = fgetcsv($handle)) !== false)
        {
            $i++;

            if ($i == 1 && $ignoreHeaderRow) {
                continue;
            }

            $events[] = [
                'ticks_since_freeze_time' => $data[0],
                'game_id'       => $data[1],
                'map_name'      => $data[2],
                'round_id'      => $data[3],
                'time_in_round' => $data[4],
                'team_name'     => $data[5],
                'location_x'    => $data[6],
                'location_y'    => $data[7],
            ];
        }

        return $events;
    }
}