<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Form\SimilarEventsType;

/**
 * Default controller
 *
 * @Route("/")
 */
class DefaultController extends Controller
{
    const EVENTS_FILENAME = 'events.csv';

    /**
     * @Route("/", name="index")
     * @Template()
     */
    public function indexAction()
    {
        return [];
    }

    /**
     * @Route("/similar-events", name="similar_events")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function similarEventsAction(Request $request)
    {
        $similarEvents = [];

        $form = $this->createForm(new SimilarEventsType());
        $form->handleRequest($request);

        if ($form->isValid())
        {
            // move the data to our upload directory and read in the events
            $file = $form->get('eventsFile')->getData();
            $file->move(__DIR__.'/../Service/data', self::EVENTS_FILENAME);

            $events = $this->get('game.event_reader')->readCsv(self::EVENTS_FILENAME);

            // get the sorted list of similar events
            $similarEvents = $this->get('game.event_comparison')->compareToPosition(
                $events,
                $form->get('x')->getData(),
                $form->get('y')->getData(),
                $form->get('numberOfEvents')->getData()
            );
        }

        return [
            'form' => $form->createView(),
            'similarEvents' => $similarEvents,
        ];
    }
}