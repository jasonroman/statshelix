<?php

namespace AppBundle\Event;

/**
 * Class for generic game events - could be used later for database/ORM selection
 */
abstract class GameEvent
{
    /**
     * @var int
     */
    public $gameId;

    /**
     * @var string
     */
    public $mapName;

    /**
     * @var int
     */
    public $roundId;

    /**
     * @var int
     */
    public $ticksSinceFreezeTime;

    /**
     * @var float
     */
    public $timeInRound;

    /**
     * @var string
     */
    public $teamName;

    /**
     * @var float
     */
    public $locationX;

    /**
     * @var float
     */
    public $locationY;

    /**
     * @var distance
     */
    public $distance;
}