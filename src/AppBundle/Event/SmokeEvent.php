<?php

namespace AppBundle\Event;

/**
 * Smoke event 
 */
class SmokeEvent extends GameEvent
{
    const EVENT_TYPE = 'smoke';
}