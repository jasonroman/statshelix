<?php

namespace AppBundle\Tests\Service;

use AppBundle\Service\GameEventComparison;

/**
 * Unit tests for the GameEventComparison service
 */
class GameEventComparisonTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var GameEventComparison
     */
    private static $service;

    /**
     * Runs once before entire suite of tests
     */
    public static function setUpBeforeClass()
    {
        self::$service = new GameEventComparison();
    }

    /**
     * Runs once after entire suite of tests
     */
    public static function tearDownAfterClass()
    {
        self::$service = null;
    }

    /**
     * @dataProvider getDistanceProvider
     *
     * @param float $x1
     * @param float $x2
     * @param float $y1
     * @param float $y2
     * @param float $expected
     */
    public function testGetDistance($x1, $x2, $y1, $y2, $expected)
    {
        // compare against the distance function to double-check correctness
        $this->assertSame($expected, self::$service->getDistance($x1, $x2, $y1, $y2));
    }

    /**
     * Returns an array of x/y coordinates for comparison and their expected distance value
     *
     * @return array
     */
    public function getDistanceProvider()
    {
        return array(
            array(0, 0, 0, 0, 0.0),
            array(-7, 17, -4, 6.5, 26.196373794859472),
            array(10, 15, 20, 30, 11.180339887498949),
        );
    }
}