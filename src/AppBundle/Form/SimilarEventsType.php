<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SimilarEventsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('eventsFile', 'file', array('label' => 'Events File'))
            ->add('numberOfEvents', 'integer', array('label' => '# of Events to Return'))
            ->add('x', 'number', array('label' => 'X'))
            ->add('y', 'number', array('label' => 'Y'))
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'similar_events';
    }
}